#
# Simplified Chinese translation of Lenny release notes
# Copyright (C) 2008 Debian Chinese project
# This file is distributed under the same license as the release
# notes.
# Authors:
# chenxianren <chenxianren@gmail.com>, 2008.
# Dongsheng Song <dongsheng.song@gmail.com>, 2008-2009.
# Deng Xiyue <manphiz@gmail.com>, 2008-2009.
# LI Daobing <lidaobing@gmail.com>, 2008.
# Ji ZhengYu <zhengyuji@gmail.com>, 2009.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <byang@debian.org>, 2019.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2021-04-18 22:32-0400\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language: zh_CN\n"
"Language-Team: Chinese (Simplified) <debian-l10n-"
"chinese@lists.debian.org>\n"
"Plural-Forms: nplurals=1; plural=0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "简介"

#: ../about.rst:6
#, fuzzy
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr "本文档告知 Debian 发行版的用户，版本 |RELEASE|（代号 |RELEASENAME|）发生的主要变化。"

#: ../about.rst:9
#, fuzzy
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"本发行说明提供如何安全地从版本 |OLDRELEASE|（代号 "
"|OLDRELEASENAME|）升级到当前版本的信息，并告知用户在升级过程中可能会遇到的已知的潜在问题。"

#: ../about.rst:14
#, fuzzy
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr "您可以在 |URL-R-N-STABLE| 获取本文档的最新版本。"

#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr "注意列出所有已知的问题是不可能的，因此我们根据影响范围和严重程度选择了一部分问题加以说明。"

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"请注意，我们仅支持从 Debian 的前一版本升级（对于本次发行，即为从 |OLDRELEASENAME| "
"升级）并对其提供说明文档。如果您需要从更早的版本升级，我们建议您先查看前一版本的发行说明，并先升级到 |OLDRELEASENAME|。"

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "报告文档错误"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr "我们已尽可能地测试了本文档中描述的所有不同的升级步骤，并预测了用户可能遇到的所有问题。"

#: ../about.rst:38
#, fuzzy
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"尽管如此，如果您认为您发现了本文档的任何错误（不正确或者缺失的信息），请在 `错误追踪系统 "
"<https://bugs.debian.org/>`__ 中向 **release-notes** "
"软件包提交错误报告。您可以考虑先浏览一遍 `已有的错误报告 <https://bugs.debian.org/release-"
"notes>`__ 以避免重复报告同一问题。如果您有能力对本文档做出贡献，也欢迎您向现有的错误报告添加额外的信息。"

#: ../about.rst:47
#, fuzzy
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""
"我们鼓励在提交错误报告时提供针对本文档源代码的补丁，并对此表示由衷感谢。您可以在 `Sources for this document "
"<#sources>`__ 中获知如何获取本文档源代码。"

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "贡献升级报告"

#: ../about.rst:57
#, fuzzy
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"我们欢迎用户提供关于从 |OLDRELEASENAME| 升级到 |RELEASENAME| 的任何信息。如果您愿意分享，请在 `错误追踪系统 "
"<https://bugs.debian.org/>`__ 中向 **upgrade-reports** "
"软件包提交包含您的升级结果的错误报告。我们希望您压缩提交的所有附件（使用 ``gzip``）。"

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr "当您提交升级报告的时候，请包含以下信息："

#: ../about.rst:66
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"在升级前后，软件包数据库的状态：**dpkg** 的状态数据库在 ``/var/lib/dpkg/status``，**apt** "
"的软件包状态信息在 ``/var/lib/apt/extended_states``。您应当在升级之前先根据 :ref:`data-backup`"
" 里的说明进行备份，但您也可以在 ``/var/backups`` 中找到 ``/var/lib/dpkg/status`` 文件的备份。"

#: ../about.rst:74
#, fuzzy
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr "使用 ``script`` 创建的会话日志，如:ref:`record-session`所述。"

#: ../about.rst:77
#, fuzzy
msgid ""
"Your ``apt`` logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"``apt`` 的日志 ``/var/log/apt/term.log``，或 ``aptitude`` 的日志 "
"``/var/log/aptitude``。"

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr "在提交错误报告之前，您应该花点时间检查和删除日志中的任何敏感和/或机密信息，因为这些信息都会被发布在公开的数据库中。"

#: ../about.rst:89
msgid "Sources for this document"
msgstr "本文档的源文件"

#: ../about.rst:91
#, fuzzy
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""
"本文档的源文件格式是 reStructuredText。HTML 版本使用 <systemitem role=\"package"
"\">docbook-xsl</systemitem> 和 <systemitem "
"role=\"package\">xsltproc</systemitem> 生成。PDF 版本使用 <systemitem "
"role=\"package\">dblatex</systemitem> 或 <systemitem "
"role=\"package\">xmlroff</systemitem> 生成。可以从 *Debian 文档计划* 的 Git "
"仓库获得本发行说明的源文件。您也可以在`网页端 <https://salsa.debian.org/ddp-team/release-"
"notes/>`__分别访问这些文件，并查看它们的变更。请参考 `Debian 文档计划的版本控制系统信息页面 "
"<https://www.debian.org/doc/vcs>`__以了解访问 Git 仓库的方法。"

