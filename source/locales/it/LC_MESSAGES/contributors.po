# Italian translation of Debian release notes
# Copyright (C) 2015 by Software in the Public Interest
# This file is distributed under the same license as the Debian Squeeze
# release notes package.
# Luca Brivio <lucab83@infinito.it>, 2007
# Luca Monducci <luca.mo@tiscali.it>, 2009, 2010, 2011
# Vincenzo Campanella <vinz65@gmail.com>, 2009.
# Beatrice Torracca <beatricet@libero.it>, 2013, 2015, 2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: Debian Jessie (8.0) release notes - release-notes.po "
"8418\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-15 18:03+0100\n"
"PO-Revision-Date: 2019-07-15 15:55+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language: it\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../contributors.rst:4
msgid "Contributors to the Release Notes"
msgstr "Contributori delle note di rilascio"

#: ../contributors.rst:6
msgid "Many people helped with the release notes, including, but not limited to"
msgstr "Molte persone hanno aiutato per le note di rilascio, inclusi, ma non solo,"

#: ../contributors.rst:8
msgid ":abbr:`Adam D. Barrat (various fixes in 2013)`,"
msgstr ":abbr:`Adam D. Barrat (varie correzioni nel 2013)`,"

#: ../contributors.rst:9
msgid ":abbr:`Adam Di Carlo (previous releases)`,"
msgstr ":abbr:`Adam Di Carlo (rilasci precedenti)`,"

#: ../contributors.rst:10
msgid ":abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,"
msgstr ":abbr:`Andreas Barth aba (rilasci precedenti: 2005 - 2007)`,"

#: ../contributors.rst:11
msgid ":abbr:`Andrei Popescu (various contributions)`,"
msgstr ":abbr:`Andrei Popescu (vari contributi)`,"

#: ../contributors.rst:12
msgid ":abbr:`Anne Bezemer (previous release)`,"
msgstr ":abbr:`Anne Bezemer (rilascio precedente)`,"

#: ../contributors.rst:13
msgid ":abbr:`Bob Hilliard (previous release)`,"
msgstr ":abbr:`Bob Hilliard (rilascio precedente)`,"

#: ../contributors.rst:14
msgid ":abbr:`Charles Plessy (description of GM965 issue)`,"
msgstr ":abbr:`Charles Plessy (descrizione del problema GM965)`,"

#: ../contributors.rst:15
msgid ":abbr:`Christian Perrier bubulle (Lenny installation)`,"
msgstr ":abbr:`Christian Perrier bubulle (Installazione di Lenny)`,"

#: ../contributors.rst:16
msgid ":abbr:`Christoph Berg (PostgreSQL-specific issues)`,"
msgstr ":abbr:`Christoph Berg (Problemi specifici con PostgreSQL)`,"

#: ../contributors.rst:17
msgid ":abbr:`Daniel Baumann (Debian Live)`,"
msgstr ":abbr:`Daniel Baumann (Debian Live)`,"

#: ../contributors.rst:18
msgid ":abbr:`David Prévot taffit (Wheezy release)`,"
msgstr ":abbr:`David Prévot taffit (rilascio di Wheezy)`,"

#: ../contributors.rst:19
msgid ":abbr:`Eddy Petrișor (various contributions)`,"
msgstr ":abbr:`Eddy Petrișor (vari contributi)`,"

#: ../contributors.rst:20
msgid ":abbr:`Emmanuel Kasper (backports)`,"
msgstr ":abbr:`Emmanuel Kasper (backport)`,"

#: ../contributors.rst:21
msgid ":abbr:`Esko Arajärvi (rework X11 upgrade)`,"
msgstr ":abbr:`Esko Arajärvi (rifacimento dell'aggiornamento X11)`,"

#: ../contributors.rst:22
msgid ":abbr:`Frans Pop fjp (previous release Etch)`,"
msgstr ":abbr:`Frans Pop fjp (rilascio precedente Etch)`,"

#: ../contributors.rst:23
msgid ":abbr:`Giovanni Rapagnani (innumerable contributions)`,"
msgstr ":abbr:`Giovanni Rapagnani (innumerevoli contributi)`,"

#: ../contributors.rst:24
msgid ":abbr:`Gordon Farquharson (ARM port issues)`,"
msgstr ":abbr:`Gordon Farquharson (problemi del port ARM)`,"

#: ../contributors.rst:25
msgid ":abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,"
msgstr ":abbr:`Hideki Yamane henrich (ha contribuito e contribuisce dal 2006)`,"

#: ../contributors.rst:26
msgid ":abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,"
msgstr ":abbr:`Holger Wansing holgerw (ha contribuito e contribuisce dal 2009)`,"

#: ../contributors.rst:27
msgid ""
":abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze "
"release)`,"
msgstr ""
":abbr:`Javier Fernández-Sanguino Peña jfs (rilascio di Etch, rilascio di "
"Squeeze)`,"

#: ../contributors.rst:28
msgid ":abbr:`Jens Seidel (German translation, innumerable contributions)`,"
msgstr ":abbr:`Jens Seidel (Traduzione in tedesco, innumerevoli contributi)`,"

#: ../contributors.rst:29
msgid ":abbr:`Jonas Meurer (syslog issues)`,"
msgstr ":abbr:`Jonas Meurer (problemi di syslog)`,"

#: ../contributors.rst:30
msgid ":abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,"
msgstr ""
":abbr:`Jonathan Nieder (rilascio di Squeeze, rilascio di Wheezy)`,"

#: ../contributors.rst:31
msgid ":abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,"
msgstr ""
":abbr:`Joost van Baal-Ilić joostvb (rilascio di Wheezy, rilascio di "
"Jessie)`,"

#: ../contributors.rst:32
msgid ":abbr:`Josip Rodin (previous releases)`,"
msgstr ":abbr:`Josip Rodin (rilasci precedenti)`,"

#: ../contributors.rst:33
msgid ":abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,"
msgstr ":abbr:`Julien Cristau jcristau (rilascio di Squeeze, rilascio di Wheezy)`,"

#: ../contributors.rst:34
msgid ":abbr:`Justin B Rye (English fixes)`,"
msgstr ":abbr:`Justin B Rye (correzioni in inglese)`,"

#: ../contributors.rst:35
msgid ":abbr:`LaMont Jones (description of NFS issues)`,"
msgstr ":abbr:`LaMont Jones (descrizione dei problemi di NFS)`,"

#: ../contributors.rst:36
msgid ":abbr:`Luk Claes (editors motivation manager)`,"
msgstr ":abbr:`Luk Claes (motivatore dei redattori)`,"

#: ../contributors.rst:37
msgid ":abbr:`Martin Michlmayr (ARM port issues)`,"
msgstr ":abbr:`Martin Michlmayr (problemi del port ARM)`,"

#: ../contributors.rst:38
msgid ":abbr:`Michael Biebl (syslog issues)`,"
msgstr ":abbr:`Michael Biebl (problemi di syslog)`,"

#: ../contributors.rst:39
msgid ":abbr:`Moritz Mühlenhoff (various contributions)`,"
msgstr ":abbr:`Moritz Mühlenhoff (vari contributi)`,"

#: ../contributors.rst:40
msgid ":abbr:`Niels Thykier nthykier (Jessie release)`,"
msgstr ":abbr:`Niels Thykier nthykier (rilascio di Jessie)`,"

#: ../contributors.rst:41
msgid ":abbr:`Noah Meyerhans (innumerable contributions)`,"
msgstr ":abbr:`Noah Meyerhans (innumerevoli contributi)`,"

#: ../contributors.rst:42
msgid ""
":abbr:`Noritada Kobayashi (Japanese translation (coordination), "
"innumerable contributions)`,"
msgstr ""
":abbr:`Noritada Kobayashi (traduzione in giapponese (coordinamento), "
"innumerevoli contributi)`,"

#: ../contributors.rst:43
msgid ":abbr:`Osamu Aoki (various contributions)`,"
msgstr ":abbr:`Osamu Aoki (vari contributi)`,"

#: ../contributors.rst:44
msgid ":abbr:`Paul Gevers elbrus (buster release)`,"
msgstr ":abbr:`Paul Gevers elbrus (rilascio di Buster)`,"

#: ../contributors.rst:45
msgid ":abbr:`Peter Green (kernel version note)`,"
msgstr ":abbr:`Peter Green (nota relativa alla versione del kernel)`,"

#: ../contributors.rst:46
msgid ":abbr:`Rob Bradford (Etch release)`,"
msgstr ":abbr:`Rob Bradford (rilascio di Etch)`,"

#: ../contributors.rst:47
msgid ":abbr:`Samuel Thibault (description of d-i Braille support)`,"
msgstr ":abbr:`Samuel Thibault (descrizione del supporto per Braille)`,"

#: ../contributors.rst:48
msgid ":abbr:`Simon Bienlein (description of d-i Braille support)`,"
msgstr ":abbr:`Simon Bienlein (descrizione del supporto per Braille)`,"

#: ../contributors.rst:49
msgid ":abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,"
msgstr ":abbr:`Simon Paillard spaillar-guest (innumerevoli contributi)`,"

#: ../contributors.rst:50
msgid ":abbr:`Stefan Fritsch (description of Apache issues)`,"
msgstr ":abbr:`Stefan Fritsch (descrizione dei problemi di Apache)`,"

#: ../contributors.rst:51
msgid ":abbr:`Steve Langasek (Etch release)`,"
msgstr ":abbr:`Steve Langasek (rilascio di Etch)`,"

#: ../contributors.rst:52
msgid ":abbr:`Steve McIntyre (Debian CDs)`,"
msgstr ":abbr:`Steve McIntyre (Debian CDs)`,"

#: ../contributors.rst:53
msgid ":abbr:`Tobias Scherer (description of \"proposed-update\")`,"
msgstr ":abbr:`Tobias Scherer (descrizione di \"proposed-update\")`,"

#: ../contributors.rst:54
msgid ""
":abbr:`victory victory-guest (markup fixes, contributed and contributing "
"since 2006)`,"
msgstr ""
":abbr:`victory victory-guest (correzioni dei markup, ha contribuito e contribuisce dal 2006)`,"

#: ../contributors.rst:55
msgid ":abbr:`Vincent McIntyre (description of \"proposed-update\")`,"
msgstr ":abbr:`Vincent McIntyre (descrizione di \"proposed-update\")`,"

#: ../contributors.rst:56
msgid ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."
msgstr ""
":abbr:`W. Martin Borgert (modifiche alle note di rilascio di Lenny e "
"passaggio a DocBook XML)`."

#: ../contributors.rst:58
msgid ""
"This document has been translated into many languages. Many thanks to all"
" the translators!"
msgstr ""
"Questo documento è stato tradotto in molte lingue. Molte grazie ai "
"traduttori."

#~ msgid ""
#~ ":abbr:`Adam D. Barrat (various fixes in"
#~ " 2013)`, :abbr:`Adam Di Carlo (previous "
#~ "releases)`, :abbr:`Andreas Barth aba (previous"
#~ " releases: 2005 - 2007)`, :abbr:`Andrei "
#~ "Popescu (various contributions)`, :abbr:`Anne "
#~ "Bezemer (previous release)`, :abbr:`Bob "
#~ "Hilliard (previous release)`, :abbr:`Charles "
#~ "Plessy (description of GM965 issue)`, "
#~ ":abbr:`Christian Perrier bubulle (Lenny "
#~ "installation)`, :abbr:`Christoph Berg "
#~ "(PostgreSQL-specific issues)`, :abbr:`Daniel "
#~ "Baumann (Debian Live)`, :abbr:`David Prévot"
#~ " taffit (Wheezy release)`, :abbr:`Eddy "
#~ "Petrișor (various contributions)`, :abbr:`Emmanuel"
#~ " Kasper (backports)`, :abbr:`Esko Arajärvi "
#~ "(rework X11 upgrade)`, :abbr:`Frans Pop "
#~ "fjp (previous release Etch)`, :abbr:`Giovanni"
#~ " Rapagnani (innumerable contributions)`, "
#~ ":abbr:`Gordon Farquharson (ARM port issues)`,"
#~ " :abbr:`Hideki Yamane henrich (contributed "
#~ "and contributing since 2006)`, :abbr:`Holger"
#~ " Wansing holgerw (contributed and "
#~ "contributing since 2009)`, :abbr:`Javier "
#~ "Fernández-Sanguino Peña jfs (Etch release,"
#~ " Squeeze release)`, :abbr:`Jens Seidel "
#~ "(German translation, innumerable contributions)`,"
#~ " :abbr:`Jonas Meurer (syslog issues)`, "
#~ ":abbr:`Jonathan Nieder jrnieder@gmail.com (Squeeze"
#~ " release, Wheezy release)`, :abbr:`Joost "
#~ "van Baal-Ilić joostvb (Wheezy release,"
#~ " Jessie release)`, :abbr:`Josip Rodin "
#~ "(previous releases)`, :abbr:`Julien Cristau "
#~ "jcristau (Squeeze release, Wheezy release)`,"
#~ " :abbr:`Justin B Rye (English fixes)`, "
#~ ":abbr:`LaMont Jones (description of NFS "
#~ "issues)`, :abbr:`Luk Claes (editors motivation"
#~ " manager)`, :abbr:`Martin Michlmayr (ARM "
#~ "port issues)`, :abbr:`Michael Biebl (syslog"
#~ " issues)`, :abbr:`Moritz Mühlenhoff (various "
#~ "contributions)`, :abbr:`Niels Thykier nthykier "
#~ "(Jessie release)`, :abbr:`Noah Meyerhans "
#~ "(innumerable contributions)`, :abbr:`Noritada "
#~ "Kobayashi (Japanese translation (coordination), "
#~ "innumerable contributions)`, :abbr:`Osamu Aoki "
#~ "(various contributions)`, :abbr:`Paul Gevers "
#~ "elbrus (buster release)`, :abbr:`Peter Green"
#~ " (kernel version note)`, :abbr:`Rob "
#~ "Bradford (Etch release)`, :abbr:`Samuel "
#~ "Thibault (description of d-i Braille "
#~ "support)`, :abbr:`Simon Bienlein (description "
#~ "of d-i Braille support)`, :abbr:`Simon "
#~ "Paillard spaillar-guest (innumerable "
#~ "contributions)`, :abbr:`Stefan Fritsch (description"
#~ " of Apache issues)`, :abbr:`Steve Langasek"
#~ " (Etch release)`, :abbr:`Steve McIntyre "
#~ "(Debian CDs)`, :abbr:`Tobias Scherer "
#~ "(description of \"proposed-update\")`, "
#~ ":abbr:`victory victory-guest victory.deb@gmail.com"
#~ " (markup fixes, contributed and "
#~ "contributing since 2006)`, :abbr:`Vincent "
#~ "McIntyre (description of \"proposed-"
#~ "update\")`, and :abbr:`W. Martin Borgert "
#~ "(editing Lenny release, switch to "
#~ "DocBook XML)`."
#~ msgstr ""
#~ ":abbr:`Adam D. Barrat (varie correzioni "
#~ "nel 2013)`, :abbr:`Adam Di Carlo "
#~ "(rilasci precedenti)`, :abbr:`Andreas Barth "
#~ "aba (rilasci precedenti: 2005 - 2007)`,"
#~ " :abbr:`Andrei Popescu (vari contributi)`, "
#~ ":abbr:`Anne Bezemer (rilascio precedente)`, "
#~ ":abbr:`Bob Hilliard (rilascio precedente)`, "
#~ ":abbr:`Charles Plessy (descrizione del "
#~ "problema GM965)`, :abbr:`Christian Perrier "
#~ "bubulle (Installazione di Lenny)`, "
#~ ":abbr:`Christoph Berg (Problemi specifici con"
#~ " PostgreSQL)`, :abbr:`Daniel Baumann (Debian "
#~ "Live)`, :abbr:`David Prévot taffit (rilascio"
#~ " di Wheezy)`, :abbr:`Eddy Petrișor (vari"
#~ " contributi)`, :abbr:`Emmanuel Kasper "
#~ "(backport)`, :abbr:`Esko Arajärvi (rifacimento "
#~ "dell'aggiornamento X11)`, :abbr:`Frans Pop fjp"
#~ " (rilascio precedente Etch)`, :abbr:`Giovanni "
#~ "Rapagnani (innumerevoli contributi)`, :abbr:`Gordon"
#~ " Farquharson (problemi del port ARM)`, "
#~ ":abbr:`Hideki Yamane henrich (ha contribuito"
#~ " e contribuisce dal 2006)`, :abbr:`Holger"
#~ " Wansing holgerw (ha contribuito e "
#~ "contribuisce dal 2009)`, :abbr:`Javier "
#~ "Fernández-Sanguino Peña jfs (rilascio di"
#~ " Etch, rilascio di Squeeze)`, :abbr:`Jens"
#~ " Seidel (Traduzione in tedesco, "
#~ "innumerevoli contributi)`, :abbr:`Jonas Meurer "
#~ "(problemi di syslog)`, :abbr:`Jonathan Nieder"
#~ " jrnieder@gmail.com (rilascio di Squeeze, "
#~ "rilascio di Wheezy)`, :abbr:`Joost van "
#~ "Baal-Ilić joostvb (rilascio di Wheezy, "
#~ "rilascio di Jessie)`, :abbr:`Josip Rodin "
#~ "(rilasci precedenti)`, :abbr:`Julien Cristau "
#~ "jcristau (rilascio di Squeeze, rilascio "
#~ "di Wheezy)`, :abbr:`Justin B Rye "
#~ "(correzioni in inglese)`, :abbr:`LaMont Jones"
#~ " (descrizione dei problemi di NFS)`, "
#~ ":abbr:`Luk Claes (motivatore dei redattori)`,"
#~ " :abbr:`Martin Michlmayr (problemi del port"
#~ " ARM)`, :abbr:`Michael Biebl (problemi di"
#~ " syslog)`, :abbr:`Moritz Mühlenhoff (vari "
#~ "contributi)`, :abbr:`Niels Thykier nthykier "
#~ "(rilascio di Jessie)`, :abbr:`Noah Meyerhans"
#~ " (innumerevoli contributi)`, :abbr:`Noritada "
#~ "Kobayashi (traduzione in giapponese "
#~ "(coordinamento), innumerevoli contributi)`, "
#~ ":abbr:`Osamu Aoki (vari contributi)`, "
#~ ":abbr:`Paul Gevers elbrus (rilascio di "
#~ "Buster)`, :abbr:`Peter Green (nota relativa"
#~ " alla versione del kernel)`, :abbr:`Rob "
#~ "Bradford (rilascio di Etch)`, :abbr:`Samuel"
#~ " Thibault (descrizione del supporto per "
#~ "Braille)`, :abbr:`Simon Bienlein (descrizione "
#~ "del supporto per Braille)`, :abbr:`Simon "
#~ "Paillard spaillar-guest (innumerevoli "
#~ "contributi)`, :abbr:`Stefan Fritsch (descrizione "
#~ "dei problemi di Apache)`, :abbr:`Steve "
#~ "Langasek (rilascio di Etch)`, :abbr:`Steve "
#~ "McIntyre (Debian CDs)`, :abbr:`Tobias Scherer"
#~ " (descrizione di \"proposed-update\")`, "
#~ ":abbr:`victory victory-guest victory.deb@gmail.com"
#~ " (correzioni dei markup, ha contribuito "
#~ "e contribuisce dal 2006)`, :abbr:`Vincent "
#~ "McIntyre (descrizione di \"proposed-"
#~ "update\")`, and :abbr:`W. Martin Borgert "
#~ "(modifiche alle note di rilascio di "
#~ "Lenny e passaggio a DocBook XML)`."

